const checkAll = document.getElementById('checkboxAll');

checkAll.onchange = () => {
  document.querySelectorAll('td > input').forEach((element) => {
    element.checked = checkAll.checked;
  })
};
