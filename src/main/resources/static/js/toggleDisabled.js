const element = document.getElementById('toggleSelect');

element.onchange = () => {
  switch (element.value) {
    case 'top':
      document.getElementById('topTextbox').disabled = false;
      document.getElementById('bottomTextbox').disabled = true;
      break;
    case 'bottom':
    document.getElementById('topTextbox').disabled = true;
      document.getElementById('bottomTextbox').disabled = false;
      break;
  }
};
